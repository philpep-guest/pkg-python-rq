Source: python-rq
Section: python
Priority: optional
Maintainer: Philippe Pepiot <philippe.pepiot@logilab.fr>
Build-Depends:
 debhelper (>= 9),
 dh-python,
 python-all,
 python-setuptools,
 python-redis (>= 3.0.0),
 python-click (>= 5.0),
 python3-all,
 python3-setuptools,
 python3-redis (>= 3.0.0),
 python3-click (>= 5.0),
Standards-Version: 4.1.3
Homepage: https://python-rq.org/
X-Python-Version: >= 2.7
X-Python3-Version: >= 3.5
Vcs-Browser: https://salsa.debian.org/philpep-guest/pkg-python-rq
Vcs-Git: https://salsa.debian.org/philpep-guest/pkg-python-rq
Testsuite: autopkgtest-pkg-python

Package: python-rq
Architecture: all
Depends:
 ${python:Depends},
 ${misc:Depends},
 python-redis (>= 3.0.0),
 python-click (>= 5.0),
Description: Redis Queue (Python 2)
 RQ (Redis Queue) is a simple Python library for queueing jobs and processing
 them in the background with workers. It is backed by Redis and it is designed
 to have a low barrier to entry. It can be integrated in your web stack easily.
 .
 This package installs the library for Python 2.

Package: python3-rq
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
 python3-redis (>= 3.0.0),
 python3-click (>= 5.0),
Description: Redis Queue (Python 3)
 RQ (Redis Queue) is a simple Python library for queueing jobs and processing
 them in the background with workers. It is backed by Redis and it is designed
 to have a low barrier to entry. It can be integrated in your web stack easily.
 .
 This package installs the library for Python 3.
